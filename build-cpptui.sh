#!/bin/bash

./cygnus-build-tools/setup-conan.sh
cd cpptui
conan create .
conan upload -c cpptui/* --all -r=mikesbytes
