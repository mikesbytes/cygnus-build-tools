#!/bin/bash

conan remote add mikesbytes https://api.bintray.com/conan/maporter/mikesbytes
conan user -p $BINTRAY_KEY -r mikesbytes $BINTRAY_USER
