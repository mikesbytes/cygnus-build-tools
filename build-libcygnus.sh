#!/bin/bash

./cygnus-build-tools/setup-conan.sh
cd libcygnus
conan create .
conan upload -c libcygnus/* --all -r=mikesbytes
