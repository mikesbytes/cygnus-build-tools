#!/bin/bash

./cygnus-build-tools/setup-conan.sh

PROJ_VERSION=$(grep -o "cygnus VERSION [0-9]\{1\}.[0-9]\{1\}.[0-9]\{1\}" cygnus/CMakeLists.txt | cut -f3 -d" ")
OUT_DIR=$(pwd)
OUT_DIR="$OUT_DIR/cygnus-bin/cygnus-v$PROJ_VERSION"
echo $OUT_DIR
mkdir -p $OUT_DIR

cd cygnus
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make package
cp *.deb $OUT_DIR
cp *.tar.gz $OUT_DIR
